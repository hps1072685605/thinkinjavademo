package chapter11;

import java.util.*;

public class PriorityQueueDemo {
	public static void main(String[] args) {
		PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
		Random rand = new Random(47);
		for(int i = 0; i < 10; i++) {
			priorityQueue.offer(rand.nextInt(i + 10));
		}
		
		print(priorityQueue.iterator());
		
	}
	
	static void print(Iterator iterator) {
		while(iterator.hasNext())
			System.out.print(iterator.next() + " ");
		System.out.println();
	}
}
