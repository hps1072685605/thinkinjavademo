package chapter9;

import java.util.Random;

public class RandomDoubles {

	private static Random rand = new Random(47);
	public double next() {
		return rand.nextDouble();
	}
}
