package timeDis;

import java.awt.*;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.swing.*;

public class MyFrame extends JFrame {
	private static final int WIDTH = 400;
	private static final int HEIGHT = 200;
	private JLabel secondLabel = new JLabel();
	private JLabel minuteLabel = new JLabel();
	private JLabel hourLabel = new JLabel();
	public MyFrame() {
		setSize(400, 200);
		Toolkit kit = getToolkit();
		Dimension screenSize = kit.getScreenSize();
		setLocation((screenSize.width - WIDTH ) / 2, (screenSize.height - HEIGHT) / 2);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
		setResizable(false);
		setLayout(null);
		secondLabel.setSize(30, 30);
		secondLabel.setLocation(220, 40);
		add(secondLabel);
		Font font = new Font("宋体", Font.BOLD, 20);
		secondLabel.setFont(font);
		
		minuteLabel.setSize(80, 30);
		minuteLabel.setLocation(175, 40);
		minuteLabel.setFont(font);
		add(minuteLabel);
		
		hourLabel.setSize(80, 30);
		hourLabel.setLocation(130, 40);
		hourLabel.setFont(font);
		add(hourLabel);
	}
	
	public void setSecond(int second) {
		secondLabel.setText(String.format("%02d", second));
	}

	public void setMinute(int minute) {
		minuteLabel.setText(String.format("%02d : ", minute));
	}
	
	public void setHour(int hour) {
		hourLabel.setText(String.format("%02d : ", hour));
	}
	
	public static void main(String[] args) throws IOException {
		MyFrame frame = new MyFrame();
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				while(true) {
					Calendar c = Calendar.getInstance();
					int second = c.get(Calendar.SECOND);
					frame.setSecond(second);
					
					int minute = c.get(Calendar.MINUTE);
					frame.setMinute(minute);
					
					int hour = c.get(Calendar.HOUR_OF_DAY);
					frame.setHour(hour);
					
					frame.repaint();
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		t.start();
		
		Thread counterThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				int count = 0;
				while(true) {
					System.out.print(++count + " ");
					if(count % 15 == 0)
						System.out.println();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		counterThread.start();
		System.in.read();
		System.exit(0);
	}
}
