package chapter21.deadlock;

import java.util.concurrent.*;

public class FixedDiningPhilosophers {
	public static void main(String[] args) throws Exception {
		int ponder = 5;
		int size = 5;
		ExecutorService exec = Executors.newCachedThreadPool();
		Chopstick[] sticks = new Chopstick[size];
		for(int i = 0; i < size; i++)
			sticks[i] = new Chopstick();
		for(int i = 0; i < size; i++)
			if(i < (size - 1))
				exec.execute(new Philosoper(sticks[i], 
					sticks[(i + 1) % size], i, ponder));
			else
				exec.execute(new Philosoper(sticks[0], 
						sticks[i], i, ponder));
		TimeUnit.SECONDS.sleep(5);
		exec.shutdownNow();
	}
}
