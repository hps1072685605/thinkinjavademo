package chapter21.deadlock;

import java.util.concurrent.*;

public class DeadlockingDiningPhilosophers {
	public static void main(String[] args) throws Exception {
		int ponder = 5;
		int size = 5;
		ExecutorService exec = Executors.newCachedThreadPool();
		Chopstick[] sticks = new Chopstick[size];
		for(int i = 0; i < size; i++)
			sticks[i] = new Chopstick();
		for(int i = 0; i < size; i++)
			exec.execute(new Philosoper(sticks[i], 
					sticks[(i + 1) % size], i, 5));
		TimeUnit.SECONDS.sleep(5);
		exec.shutdownNow();
	}
}
