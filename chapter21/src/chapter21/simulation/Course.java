package chapter21.simulation;

import java.util.Random;

public enum Course {
	APPETIZER(Food.Appetizer.class),
	MAINCOURSE(Food.MainCourse.class),
	DESSERT(Food.Dessert.class),
	COFFEE(Food.Coffee.class);
	private Food[] values;
	
	private Course(Class<? extends Food> kind) {
		values = kind.getEnumConstants();
	}
	
	public Food randomSelection() {
		Random rand = new Random(47);
		return values[rand.nextInt(values.length)];
	}
}
