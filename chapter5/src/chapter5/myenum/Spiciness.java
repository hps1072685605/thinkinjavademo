package chapter5.myenum;

public enum Spiciness {

	NOT, MILD, MEDIUM, HOT, FLAMING
}
