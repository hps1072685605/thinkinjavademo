package chapter17.test;

import java.util.*;

import chapter17.CountingIntegerList;

public class ListPerformance {
	static Random rand = new Random();
	static int reps = 1000;
	static List<Test<List<Integer>>> tests = new ArrayList<>();
	static List<Test<LinkedList<Integer>>> qTests = new ArrayList<>();
	static {
		tests.add(new Test<List<Integer>>("add") {
			@Override
			int test(List<Integer> list, TestParam tp) {
				int loops = tp.loops;
				int listSize = tp.size;
				for(int i = 0; i < loops; i++) {
					list.clear();
					for(int j = 0; j < listSize; j++)
						list.add(j);
				}
				return loops * listSize;
			}
		});
		
		tests.add(new Test<List<Integer>>("get") {
			@Override
			int test(List<Integer> list, TestParam tp) {
				int loops = tp.loops * reps;
				int listSize = list.size();
				for(int i = 0; i < loops; i++) {
					list.get(rand.nextInt(listSize));
				}
				return loops;
			}
		});
		
		tests.add(new Test<List<Integer>>("set") {
			@Override
			int test(List<Integer> list, TestParam tp) {
				int loops = tp.loops * reps;
				int listSize = list.size();
				for(int i = 0; i < loops; i++) {
					list.set(rand.nextInt(listSize), 47);
				}
				return loops;
			}
		});
	}
	
	static class ListTester extends Tester<List<Integer>> {

		public ListTester(List<Integer> container, List<Test<List<Integer>>> tests) {
			super(container, tests);
		}
		
		@Override
		protected List<Integer> initialize(int size) {
			container.clear();
			container.addAll(new CountingIntegerList(size));
			return container;
		}
		
		public static void run(List<Integer> list,
				List<Test<List<Integer>>> tests) {
			new ListTester(list, tests);
		}
	}
	
	public static void main(String[] args) {
		if(args.length > 0)
			Tester.defaultParams = TestParam.array(args);
		Tester<List<Integer>> arrayTest = 
				new Tester<List<Integer>>(null, tests.subList(1, 3)){
			@Override
			protected List<Integer> initialize(int size) {
				 
				return Arrays.asList(1, 5, 9, 7, 0, 2, 6, 3, 8, 4);
			}
		};
		
		arrayTest.setHeadline("Array as List");
		arrayTest.timedTest();
	}
}
