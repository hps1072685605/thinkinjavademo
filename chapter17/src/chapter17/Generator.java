package chapter17;

public interface Generator<T> {
	T next();
}
