package chapter20.annotations.database;

@DBTable(name = "MEMBER")
public class Member {
	@SQLString(30)
	String firsrName;
	@SQLString(50)
	String lastName;
	@SQLInteger
	String age;
	@SQLString(value = 30, constraints = @Constraints(primaryKey = true))
	String handle;
	static int memberCount;

	public String getFirsrName() {
		return firsrName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getAge() {
		return age;
	}

	public String getHandle() {
		return handle;
	}

	@Override
	public String toString() {
		return handle;
	}
}
