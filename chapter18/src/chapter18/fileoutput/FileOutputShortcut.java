package chapter18.fileoutput;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;

import chapter18.fileinput.BufferedInputFile;

public class FileOutputShortcut {
	static String file = "FileOutputShortcut.out";
	public static void main(String[] args) throws Exception{
		BufferedReader in = new BufferedReader(
				new FileReader("/home/test/javaio/test.txt"));
		PrintWriter out = new PrintWriter(file);
		int lineCount = 1;
		String s;
		while((s = in.readLine()) != null)
			out.println(lineCount++ + ": " + s);
		out.close();
		System.out.println(BufferedInputFile.read(file));
	}
}
