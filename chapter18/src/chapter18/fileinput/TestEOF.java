package chapter18.fileinput;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class TestEOF {
	public static void main(String[] args) throws Exception {
		DataInputStream in = new DataInputStream(
				new BufferedInputStream(
						new FileInputStream("/home/test/javaio/test.txt")));
		while(in.available() != 0)
			System.out.print((char)in.readByte());
	}
}
