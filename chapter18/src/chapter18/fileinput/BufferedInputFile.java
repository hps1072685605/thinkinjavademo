package chapter18.fileinput;

import java.io.*;

public class BufferedInputFile {
	public static String read(String fileName) throws Exception {
		BufferedReader in = new BufferedReader(
				new FileReader(fileName));
		String s;
		StringBuffer sb = new StringBuffer();
		while((s = in.readLine()) != null) {
			sb.append(s + "\n");
		}
		in.close();
		return sb.toString();
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println(read("/home/test/javaio/test.txt"));
	}
}
