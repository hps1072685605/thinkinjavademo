package chapter18.fileinput;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.UnsupportedEncodingException;

public class FormattedMemoryInput {

	public static void main(String[] args) {
		try {
			DataInputStream in = new DataInputStream(
					new ByteArrayInputStream(
							BufferedInputFile.read("/home/test/javaio/test.txt").getBytes("utf-8")));
			while(true)
				System.out.print((char)in.readByte());
		} catch (EOFException e) {
			System.err.println("End of stream");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
