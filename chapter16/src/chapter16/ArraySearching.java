package chapter16;

import java.util.Arrays;
import java.util.Random;

public class ArraySearching {
	public static void main(String[] args) {
		Random r = new Random(47);
		int[] a = new int[25];
		for(int i = 0; i < a.length; i++) 
			a[i] = r.nextInt(1000);
		Arrays.sort(a);
		System.out.println("Sorted array: " + Arrays.toString(a));
		while(true) {
			int k = r.nextInt(1000);
			int location = Arrays.binarySearch(a, k);
			if(location >= 0) {
				System.out.println("Location of " + k + " is " + location +
						", a[" + location + "] =" + k);
				break;
			}
		}
	}
}
